from flask import (
    Blueprint,
    render_template,
    flash,
    redirect,
    url_for,
    request,
)
from Controls import LoginControl
from App import (
    bcrypt,
    db,
)
from App.models import (
    User,
    Category,
    Task,
    Buyer,
    Seen,

)
from App.forms import (
    RegisterForm,
    UserEditFrom,
    CategoryForm,
    BuyerForm,
    TaskForm,
)

admin = Blueprint("admin", __name__, static_folder="static", template_folder="/templates")


@admin.route("/")
def index():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        return render_template("admin/index.html", title="Dashboard", user=valid[3])
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/user/add', methods=["POST", "GET"])
def add_user():
    # valid = LoginControl.validation()
    # if valid[0] and valid[2] == "admin":
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_pass = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(name=form.name.data, username=form.username.data, email=form.email.data, address=form.address.data,
                    phone=form.phone.data, password=hashed_pass, type=form.type.data)
        db.session.add(user)
        db.session.commit()
        flash('Account has been created!', 'success')
        return redirect(url_for('admin.user_list'))
    return render_template('admin/add_user.html', title="Dashboard", user="admin", form=form)
    # else:
    #     return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/users')
def user_list():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        users = User.query.all()
        return render_template('admin/users.html', title="User List", user=valid[3], user_data=users)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/profile')
def profile():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        user_data = User.query.get_or_404(valid[1])
        return render_template('admin/profile.html', title="Profile", user=valid[3], profile=user_data)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('user/<int:user_id>')
def user_profile(user_id):
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        user_data = User.query.get_or_404(user_id)
        return render_template('admin/user_profile.html', title="Profile", user=valid[3], profile=user_data)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('suspend/<int:user_id>')
def suspend_user(user_id):
    valid = LoginControl.validation()
    user = User.query.get_or_404(user_id)
    if valid[0] and valid[2] == "admin":
        user.type = "NULL"
        db.session.commit()
        flash("User's Access Has been suspended!", "info")
        return redirect(url_for('admin.user_list'))
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('delete/<int:user_id>')
def delete_user(user_id):
    valid = LoginControl.validation()
    user = User.query.get_or_404(user_id)
    if valid[0] and valid[2] == "admin":
        db.session.delete(user)
        db.session.commit()
        flash("User has been deleted!", "info")
        return redirect(url_for('admin.user_list'))
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/edit/user/<int:user_id>', methods=["POST", "GET"])
def edit_user(user_id):
    valid = LoginControl.validation()
    user = User.query.get_or_404(user_id)
    form = UserEditFrom()
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            user.name = form.name.data
            user.username = form.username.data
            user.email = form.email.data
            user.address = form.address.data
            user.phone = form.phone.data
            user.type = form.type.data
            db.session.commit()
            flash("User has been edited!", "info")
            return redirect(url_for('admin.user_list'))
        elif request.method == "GET":
            form.name.data = user.name
            form.username.data = user.username
            form.email.data = user.email
            form.address.data = user.address
            form.phone.data = user.phone
            form.type.data = user.type
            return render_template('admin/edit_user.html', title="Edit User", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/edit/profile', methods=["POST", "GET"])
def edit_profile():
    valid = LoginControl.validation()
    user = User.query.get_or_404(valid[1])
    form = UserEditFrom()
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            user.name = form.name.data
            user.username = form.username.data
            user.email = form.email.data
            user.address = form.address.data
            user.phone = form.phone.data
            user.type = form.type.data
            db.session.commit()
            flash("Profile Has been Updated", "info")
            return redirect(url_for('admin.edit_profile'))
        elif request.method == "GET":
            form.name.data = user.name
            form.username.data = user.username
            form.email.data = user.email
            form.address.data = user.address
            form.phone.data = user.phone
            form.type.data = user.type
            return render_template('admin/edit_user.html', title="Edit Profile ", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/category/add', methods=["POST", "GET"])
def add_cat():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        form = CategoryForm()
        if form.validate_on_submit():
            category = Category(name=form.name.data, created_by=valid[1])
            db.session.add(category)
            db.session.commit()
            flash('Category has been created!', 'success')
            return redirect(url_for('admin.cat_list'))
        return render_template('admin/task/add_cat.html', title="Add Category", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/category')
def cat_list():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        cats = Category.query.all()
        return render_template('admin/task/cats.html', title="Category List", user=valid[3], cat_data=cats)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('delete/category/<int:cat_id>')
def delete_cat(cat_id):
    valid = LoginControl.validation()
    cat = Category.query.get_or_404(cat_id)
    if valid[0] and valid[2] == "admin":
        db.session.delete(cat)
        db.session.commit()
        flash("Category has been deleted!", "info")
        return redirect(url_for('admin.cat_list'))
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('edit/category/<int:cat_id>', methods=["POST", "GET"])
def edit_category(cat_id):
    valid = LoginControl.validation()
    cat = Category.query.get_or_404(cat_id)
    form = CategoryForm()
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            cat.name = form.name.data
            db.session.commit()
            flash("Category Has been Updated", "info")
            return redirect(url_for('admin.cat_list'))
        elif request.method == "GET":
            form.name.data = cat.name
            return render_template('admin/task/edit_cat.html', title="Edit Category ", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/buyer/add', methods=["POST", "GET"])
def add_buyer():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        form = BuyerForm()
        if form.validate_on_submit():
            buyer = Buyer(name=form.name.data, created_by=valid[1])
            db.session.add(buyer)
            db.session.commit()
            flash('Buyer has been added!', 'success')
            return redirect(url_for('admin.buyer_list'))
        return render_template('admin/task/add_buyer.html', title="Add Buyer", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/buyer')
def buyer_list():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        buyer = Buyer.query.filter_by(created_by=valid[1]).all()
        return render_template('admin/task/buyers.html', title="Buyer List", user=valid[3], buyer_data=buyer)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('edit/buyer/<int:buyer_id>', methods=["POST", "GET"])
def edit_buyer(buyer_id):
    valid = LoginControl.validation()
    buyer = Buyer.query.get_or_404(buyer_id)
    form = BuyerForm()
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            buyer.name = form.name.data
            db.session.commit()
            flash("Buyer Name Has been Updated", "info")
            return redirect(url_for('admin.buyer_list'))
        elif request.method == "GET":
            form.name.data = buyer.name
            return render_template('admin/task/edit_buyer.html', title="Edit Buyer ", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('delete/buyer/<int:buyer_id>')
def delete_buyer(buyer_id):
    valid = LoginControl.validation()
    buyer = Buyer.query.get_or_404(buyer_id)
    if valid[0] and valid[2] == "admin":
        db.session.delete(buyer)
        db.session.commit()
        flash("Buyer has been deleted!", "info")
        return redirect(url_for('admin.buyer_list'))
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/task/add', methods=["POST", "GET"])
def add_task():
    valid = LoginControl.validation()
    form = TaskForm()
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            task = Task(title=form.title.data, description=form.description.data, due_date=form.due_date.data,
                        assigned=form.assigned.data, category_id=form.category.data, buyer_id=form.buyer.data,
                        created_by=valid[1], status="1", payment="0", work_amount="0",
                        chat_room="test")
            db.session.add(task)
            db.session.commit()
            flash("Task has been created", "success")
            return redirect(url_for('admin.task_all'))
        else:
            form.category.choices = [(category.id, category.name) for category in Category.query.all()]
            form.buyer.choices = [(buyer.id, buyer.name) for buyer in Buyer.query.all()]
            form.assigned.choices = [(user.id, user.name) for user in User.query.filter_by(type="user").all()]
            return render_template("admin/task/add_task.html", title="Add Task", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('delete/task/<int:task_id>')
def delete_task(task_id):
    valid = LoginControl.validation()
    task = Task.query.get_or_404(task_id)
    if valid[0] and valid[2] == "admin":
        db.session.delete(task)
        db.session.commit()
        flash("Task has been deleted!", "info")
        return redirect(url_for('admin.task_all'))
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('edit/task/<int:task_id>', methods=["POST", "GET"])
def edit_task(task_id):
    valid = LoginControl.validation()
    task = Task.query.get_or_404(task_id)
    form = TaskForm(assigned=task.assigned, buyer=task.buyer_id, category=task.category_id)
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            task.title = form.title.data
            task.description = form.description.data
            task.due_date = form.due_date.data
            task.assigned = form.assigned.data
            task.buyer_id = form.buyer.data
            task.category_id = form.category.data

            db.session.commit()
            flash("Task Has been Updated", "info")
            return redirect(url_for('admin.task_all'))
        elif request.method == "GET":

            form.category.choices = [(category.id, category.name) for category in Category.query.all()]
            form.buyer.choices = [(buyer.id, buyer.name) for buyer in Buyer.query.all()]
            form.assigned.choices = [(user.id, user.name) for user in User.query.filter_by(type="user").all()]

            form.title.data = task.title
            form.description.data = task.description
            form.due_date.data = task.due_date

            return render_template('admin/task/edit_task.html', title="Edit Task ", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


# @admin.route('/task/completed/<int:task_id>')
# def completed_task(task_id):
#     valid = LoginControl.validation()
#     task = Task.query.get_or_404(task_id)
#     if valid[0] and valid[2] == "admin":
#         if request.method == "POST":
#             task.status = "0"
#             db.session.commit()
#             flash("Task has been marked as Completed!", 'info')
#             return redirect(url_for('admin.task_all'))
#         else:
#             pass


@admin.route('duplicate/task/<int:task_id>', methods=["POST", "GET"])
def duplicate_task(task_id):
    valid = LoginControl.validation()
    task = Task.query.get_or_404(task_id)
    form = TaskForm(assigned=task.assigned, buyer=task.buyer_id, category=task.category_id)
    if valid[0] and valid[2] == "admin":
        if request.method == "POST":
            dup_task = Task(title=form.title.data, description=form.description.data, due_date=form.due_date.data,
                            assigned=form.assigned.data, category_id=form.category.data, buyer_id=form.buyer.data,
                            created_by=valid[1], status="1", payment="0", work_amount="0",
                            chat_room="test")
            db.session.add(dup_task)
            db.session.commit()
            flash("Task has been created", "success")
            return redirect(url_for('admin.task_all'))
        else:
            form.category.choices = [(category.id, category.name) for category in Category.query.all()]
            form.buyer.choices = [(buyer.id, buyer.name) for buyer in Buyer.query.all()]
            form.assigned.choices = [(user.id, user.name) for user in User.query.filter_by(type="user").all()]

            form.title.data = task.title
            form.description.data = task.description
            form.due_date.data = task.due_date
            return render_template('admin/task/duplicate_task.html', title="Duplicate Task ", user=valid[3], form=form)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/task/active')
def task_active():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        task = Task.query.filter_by(status="1", created_by=valid[1]).all()
        return render_template("admin/task/active_tasks.html", title="Active Task", user=valid[3], tasks=task)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/task/all')
def task_all():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        task = Task.query.filter_by(created_by=valid[1]).all()
        return render_template("admin/task/tasks.html", title="All Task", user=valid[3], tasks=task)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/task/inactive')
def task_inactive():
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        task = Task.query.filter_by(created_by=valid[1], status=0).all()
        return render_template("admin/task/inactive_tasks.html", title="Finished Task", user=valid[3], tasks=task)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/buyer/<int:buyer_id>')
def buyer_task(buyer_id):
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        task_num = Task.query.filter_by(buyer_id=buyer_id).count()
        tasks = Task.query.filter_by(buyer_id=buyer_id).all()
        buyer = Buyer.query.get_or_404(buyer_id)
        return render_template("admin/task/task_by_buyer.html", title=f"{buyer.name}'s Profile", user=valid[3],
                               tasks=tasks, task_num=task_num, buyer=buyer)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@admin.route('/category/<int:cat_id>')
def cat_task(cat_id):
    valid = LoginControl.validation()
    if valid[0] and valid[2] == "admin":
        task_num = Task.query.filter_by(category_id=cat_id).count()
        tasks = Task.query.filter_by(category_id=cat_id).all()
        cat = Category.query.get_or_404(cat_id)
        return render_template("admin/task/task_by_cat.html", title=f"{cat.name}' Jobs", user=valid[3], tasks=tasks,
                               task_num=task_num, cat=cat)
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)
