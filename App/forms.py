from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    BooleanField,
    SelectField,
    DateField,
    TextAreaField,
    FileField,
)
from wtforms.validators import (
    DataRequired,
    Length,
    Email,
    ValidationError,
    EqualTo,
)
from App.models import (
    User,
    Category,
    Buyer,
)


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=3, max=8)])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me')
    submit = SubmitField('Sign In')


class RegisterForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired(), Length(min=3, max=8)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    address = StringField('Address', validators=[DataRequired()])
    phone = StringField('Phone', validators=[DataRequired()])
    type = SelectField('Type', validators=[DataRequired()], choices=[('admin', 'Admin'), ('user', 'User')])
    submit = SubmitField('Submit')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Username has been taken, Please use another one!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email has been taken, Please use another one!')


class UserEditFrom(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired(), Length(min=3, max=8)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    address = StringField('Address', validators=[DataRequired()])
    phone = StringField('Phone', validators=[DataRequired()])
    type = SelectField('Type', validators=[DataRequired()], choices=[('admin', 'Admin'), ('user', 'User')])
    submit = SubmitField('Submit')


class TaskForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    description = TextAreaField('Description')
    file = FileField('Choose File')
    due_date = DateField('Due Date', validators=[DataRequired()])
    # from DB assign, cats and buyer
    assigned = SelectField('Assign User', validators=[DataRequired()], choices=[])
    buyer = SelectField('Buyer', validators=[DataRequired()], choices=[])
    category = SelectField('Work Category', validators=[DataRequired()], choices=[])
    submit = SubmitField('Submit')


class TaskCompleteForm(FlaskForm):
    description = TextAreaField('Description')
    file = FileField('Choose File')
    submit = SubmitField('Submit')


class CategoryForm(FlaskForm):
    name = StringField('Category Name', validators=[DataRequired()])
    submit = SubmitField('Submit')

    def validate_name(self, name):
        cat = Category.query.filter_by(name=name.data).first()
        if cat:
            raise ValidationError('Category name has been taken, Please use another one!')


class BuyerForm(FlaskForm):
    name = StringField('Buyer Name', validators=[DataRequired()])
    submit = SubmitField('Submit')

    def validate_name(self, name):
        buyer = Buyer.query.filter_by(name=name.data).first()
        if buyer:
            raise ValidationError('Buyer name has been taken, Please use another one!')


class RequestTokenFrom(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=3, max=12)])
    submit = SubmitField('Request Reset Password')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is None:
            raise ValidationError('Account with that Username is not in our Database, Please try again'
                                  ' or contact Admin!')


class PasswordResetForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')


class ChangePassword(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    new_password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('new_password')])
    submit = SubmitField('Change Password')


class ChangeUsername(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=3, max=12)])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Change Username')