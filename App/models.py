from App import db, app
from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    address = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    profile_pic = db.Column(db.String(20), nullable=False, default='default_user.jpg')
    password = db.Column(db.String(60), nullable=False)
    type = db.Column(db.String(10), nullable=False)

    assigned = db.relationship('Task', backref='task_assigned', lazy=True)
    cats = db.relationship('Category', backref='cat_creator', lazy=True)
    buyer = db.relationship('Buyer', backref='buyer_creator', lazy=True)

    def get_reset_token(self, expire_in=1800):
        s = Serializer(app.config['SECRET_KEY'], expire_in)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod
    def verify_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)
        except:
            return None
        return user_id

    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.profile_pic}', '{self.address}, '{self.phone}')"


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    tasks = db.relationship('Task', backref='category', lazy=True)

    def __repr__(self):
        return f"Category('{self.name}')"


class Buyer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    created_by = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    tasks = db.relationship('Task', backref='buyer', lazy=True)

    def __repr__(self):
        return f"Buyer('{self.name}')"


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text, nullable=False)
    work_amount = db.Column(db.String(20), nullable=False)
    date_created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    due_date = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    payment = db.Column(db.Integer, nullable=False)
    chat_room = db.Column(db.String(255), nullable=False, unique=True)
    file = db.Column(db.String(20), nullable=False, default="default.file")
    created_by = db.Column(db.Integer, nullable=False)

    assigned = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.id'), nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)

    seen = db.relationship('Seen', backref='tasks', lazy=True)
    paid = db.relationship('Payment', backref='tasks', lazy=True)
    completed = db.relationship('CompleteTask', backref='completed_task', lazy=True)

    def __repr__(self):
        return f"Task('{self.title}, {self.date_created}, {self.due_date}')"


class Seen(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    task_seen = db.Column(db.Integer, nullable=False, default='1')
    chat_seen = db.Column(db.Integer, nullable=False, default='1')
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'), nullable=False)

    def __repr__(self):
        return f"Task('{self.task_seen}, {self.chat_seen}, {self.task_id}')"


class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Integer, nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'), nullable=False)

    def __repr__(self):
        return f"Payment('{self.status}')"


class CompleteTask(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text, nullable=False)
    file = db.Column(db.String(20), nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'), nullable=False)

    def __repr__(self):
        return f"Payment('{self.description}, {self.file}')"

