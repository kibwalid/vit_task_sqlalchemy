from flask import (
    render_template,
    redirect,
    url_for,
    flash,

)
from Controls import LoginControl
from App.forms import LoginForm
from App.models import (
    User,
)
from App.admin_view.admin import admin
from App.user_view.user import user
from App.settings_view.settings import settings
from App import app, bcrypt

app.register_blueprint(admin, url_prefix="/admin")
app.register_blueprint(user, url_prefix="/user")
app.register_blueprint(settings, url_prefix="/settings")


@app.errorhandler(404)
def not_found(e):
    return render_template("main/error.html", error=e)


@app.route('/')
def index():
    return redirect(url_for('login'))


@app.route('/login', methods=["POST", "GET"])
def login():

    valid = LoginControl.validation()
    if valid[0]:
        return redirect(url_for(f'{valid[2]}.index'))
    else:
        form = LoginForm()
        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            if user and bcrypt.check_password_hash(user.password, form.password.data):
                LoginControl.add_session(user.id, user.type, user.name, form.remember.data)
                if user.type == "admin":
                    return redirect(url_for('admin.index'))
                elif user.type == "user":
                    return redirect(url_for('user.index'))
                else:
                    flash('Your account access has been suspended!', 'info')
            else:
                flash('Username and Password does not match our database!', 'info')
        return render_template("main/login.html", title="Login", form=form)


@app.route('/logout')
def logout():
    valid = LoginControl.validation()
    if valid[0]:
        LoginControl.pop_session()
        flash("You have been logged out!")
        return redirect(url_for('.login'))
    else:
        flash("You're already logged out!", "info")
        return redirect(url_for('.login'))
