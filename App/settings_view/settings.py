from flask import (
    Blueprint,
    render_template,
    flash,
    redirect,
    url_for,
    request,
)
from flask_mail import Message
from Controls import LoginControl
from App import mail, bcrypt, db
from App.models import (
    User,
)
from App.forms import (
    RequestTokenFrom,
    PasswordResetForm,
    ChangePassword,
    ChangeUsername,
)

settings = Blueprint("settings", __name__, static_folder="static", template_folder="/templates")


@settings.route('/')
def index():
    valid = LoginControl.validation()
    if valid[0]:

        return render_template("settings/index.html", title="Settings", user=valid[3])
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@settings.route('/password/change', methods=['GET', 'POST'])
def change_password():
    valid = LoginControl.validation()
    if valid[0]:
        form = ChangePassword()
        if request.method == "POST":
            user = User.query.get_or_404(valid[1])
            new_hashed_pass = bcrypt.generate_password_hash(form.new_password.data).decode('utf-8')
            if bcrypt.check_password_hash(user.password, form.password.data):
                user.password = new_hashed_pass
                db.session.commit()
                flash('Your password has been changed!')
                return redirect(url_for('settings.index'))
            else:
                flash('Old Password does not match our database')
                return redirect(url_for('settings.change_password'))
        else:
            return render_template('settings/change_pass.html', form=form, titile="Change Password")
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


@settings.route('/username/change', methods=['GET', 'POST'])
def change_username():
    valid = LoginControl.validation()
    if valid[0]:
        form = ChangeUsername()
        if request.method == "POST":
            user = User.query.get_or_404(valid[1])
            if bcrypt.check_password_hash(user.password, form.password.data):
                user.username = form.username.data
                db.session.commit()
                flash('Your username has been changed!')
                return redirect(url_for('settings.index'))
            else:
                flash('Password does not match our database')
                return redirect(url_for('settings.change_username'))
        else:
            return render_template('settings/change_username.html', form=form, titile="Change Password")
    else:
        return render_template("main/error.html", error=LoginControl.ErrMsg.access)


# Reset Password - Unlogged
def send_reset_email(user):
    token = User().get_reset_token()
    msg = Message('Password Reset Request', sender='noreply@demo.com', recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link: 
    {url_for('settings.reset_password', token=token, _external=True)} 
    
    If you did not make this request please ignore this mail and no changes will happen.
    '''
    mail.send(msg)


@settings.route('/reset/password', methods=['GET', 'POST'])
def reset_request():
    valid = LoginControl.validation()
    if valid[0]:
        return redirect(url_for(f'{valid[2]}.index'))
    else:
        form = RequestTokenFrom()
        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            send_reset_email(user)
            flash('Please check your email to reset your password!', 'info')
            return redirect(url_for('login'))
        return render_template('settings/reset_token.html', form=form, title="Reset Password")


@settings.route('/reset/password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    valid = LoginControl.validation()
    if valid[0]:
        return redirect(url_for(f'{valid[2]}.index'))
    else:
        token_data = User().verify_token(token)

        if token_data:
            user_id = token_data['user_id']
            user = User.query.get(user_id)
            form = PasswordResetForm()
            if form.validate_on_submit():
                hashed_pass = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
                user.password = hashed_pass
                db.session.commit()
                flash('Your password has been reset, you can now login !', 'info')
                return redirect(url_for('login'))
            return render_template('settings/reset_password.html', form=form, title="Reset Password")
        else:
            flash('This is an invalid or expired token.', 'warning')
            return redirect(url_for('settings.reset_request'))


