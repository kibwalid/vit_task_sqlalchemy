from flask import (
    Blueprint,
)

user = Blueprint("user", __name__, static_folder="static", template_folder="templates")


@user.route("/")
def index():
    return "<h1>You're not logged in yet!</h1>"


